package eskimidsp.automation.main;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import eskimidsp.automation.Utils.DriverClass;
import eskimidsp.automation.Utils.IdentifierDepot;

public class MainTask {
	@Test
	public void TasksToDo() throws InterruptedException {
		WebDriver runner = DriverClass.getDriver();
		runner.get(IdentifierDepot.Google);
		runner.manage().window().maximize();
		
		runner.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
		
		WebElement googleSearchBar=runner.findElement(By.name(IdentifierDepot.GSearchBar));
		googleSearchBar.sendKeys("dsp.eskimi.com");
		googleSearchBar.submit();
		
		runner.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
		WebElement eskimiurl = runner.findElement(By.xpath(IdentifierDepot.EskimiUrl));		
		Actions act = new Actions(runner);
		act.keyDown(Keys.SHIFT).click(eskimiurl).keyUp(Keys.SHIFT).build().perform();
       
		Thread.sleep(1000);
		for(String winHandle : runner.getWindowHandles()){
    	    runner.switchTo().window(winHandle);
    	}
       	Thread.sleep(1000);
		runner.manage().window().maximize();
		
		runner.findElement(By.xpath(IdentifierDepot.Username)).sendKeys("sqa-demo");
		runner.findElement(By.name(IdentifierDepot.Password)).sendKeys("demo123");
		runner.findElement(By.xpath(IdentifierDepot.Login)).click();
		
		WebDriverWait wait1=new WebDriverWait(runner, 20); 
		WebElement dropdownToggle= wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id(IdentifierDepot.SmallMenu)));
		dropdownToggle.click();
		
		
		WebElement preview= runner.findElement(By.linkText(IdentifierDepot.Preview));
		act.moveToElement(preview);
		preview.click();
		
		
		ArrayList<String> tabs = new ArrayList<String>(runner.getWindowHandles());
		runner.switchTo().window(tabs.get(2));
		
		String currenturlontab= runner.getCurrentUrl();
	    System.out.println(currenturlontab);
	   
	    runner.switchTo().window(tabs.get(1));
	    
	    dropdownToggle.click();
	    act.moveToElement(preview);
	    preview.click();
	    
	    runner.switchTo().window(tabs.get(2));
	    
	    String currenturlontab1= runner.getCurrentUrl();
	    System.out.println(currenturlontab);
	    
	    Assert.assertEquals(currenturlontab1, currenturlontab);
	    
	    runner.quit();
	    
		

	}

}
